import re
import json


with open("parameters_pipeline-test.json", "r") as file:

  data=json.load(file)

print(data["VMs"][0]["VM_NAME"])

print(len(data["VMs"]))

filename = "filehost_%s" %data["VMs"][0]["Customer"]


with open(filename, "w") as f:
  number_VM=len(data["VMs"])
  for i in range(number_VM):
    f.write(data["VMs"][i]["IPV4"]+ " "+ ' hostname=' + data["VMs"][i]["VM_NAME"] + " ipv4=" + data["VMs"][i]["IPV4"]+ " flavor=X " + " volume=" + data["VMs"][i]["VOLUME"] )
    f.write('\n')
