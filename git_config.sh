#!/bin/bash

# push generated config from runner backup to gitlab repo


git config user.email "georgia.feideropoulo@gmail.com"
git config user.name "yiol"

git add --all
git commit -m "config files regenerated `date`"

# a project token is used for git authentication. The token automatically generates an associated bot gitlab user: 
git push https://yiol:${test2}@gitlab.com/yiol/diakopes.git HEAD:main
